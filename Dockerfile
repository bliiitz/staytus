FROM ruby
MAINTAINER Tim Perry <pimterry@gmail.com>

USER root

RUN apt-get update && \
    apt-get install -y nodejs
    
COPY . /opt/staytus

RUN cd /opt/staytus && \
    bundle install --deployment --without development:test

ENTRYPOINT /opt/staytus/docker-start.sh

EXPOSE 5000
