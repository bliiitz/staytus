#!/bin/bash
cd /opt/staytus

cp config/database.example.yml config/database.yml
sed -i "s/username:.*/username: $DB_USER/" config/database.yml
sed -i "s|password:.*|password: $DB_PASSWORD|" config/database.yml
sed -i "s|database:.*|database: $DB_NAME|" config/database.yml
sed -i "s|host:.*|host: $DB_HOST|" config/database.yml

bundle exec rake staytus:build staytus:upgrade
bundle exec foreman start
